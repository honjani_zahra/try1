/*****************************************************************************
* FILE: 4-join.c
* DESCRIPTION:
*   This example demonstrates how to "wait" for thread completions by using
*   the Pthread join routine.  Threads are explicitly created in a joinable
*   state for portability reasons. Use of the pthread_exit status argument is 
*   also shown.
* AUTHOR: Blaise Barney
* LAST REVISED: 2020-04-16 Mohammad Moridi
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <pthread.h>

#define NUMBER_OF_THREADS 4
#define RESET_COLOR_INDEX 4

char COLOR_CODES[5][255] =
{
	"\033[1m\033[30m",		// Bold BLACK
	"\033[1m\033[31m",		// Bold RED
	"\033[1m\033[32m",		// Bold GREEN
	"\033[1m\033[34m",		// Bold BLUE
	"\x1B[0m"				// Reset
};

void* busy_work(void* tid)
{
	long thread_id;
	char result_str[255];
	double result = 0.0;
	
	thread_id = (long)tid;
	
	sprintf(result_str, "%sThread %ld starting...%s\n",
			COLOR_CODES[thread_id], thread_id,
			COLOR_CODES[RESET_COLOR_INDEX]);
	printf("%s", result_str);
	
	for (int i = 0; i < 1e6; i++)
		result = result + sin(i) * tan(i);

	sprintf(result_str, "%sThread %ld done. Result = %e%s\n",
			COLOR_CODES[thread_id], thread_id, result,
			COLOR_CODES[RESET_COLOR_INDEX]);
	printf("%s", result_str);

	pthread_exit((void*)thread_id);
}

int main ()
{
	pthread_t thread[NUMBER_OF_THREADS];
	int return_code;
	void* status;

	for(long tid = 0; tid < NUMBER_OF_THREADS; tid++)
	{
		printf("Main: creating thread %ld\n", tid);

		return_code = pthread_create(&thread[tid], NULL,
				busy_work, (void*)tid); 

		if (return_code)
		{
			printf("ERROR; return code from pthread_create() is %d\n",
					return_code);
			exit(-1);
		}
	}

	for(long tid = 0; tid < NUMBER_OF_THREADS; tid++)
	{
		return_code = pthread_join(thread[tid], &status);
		if (return_code)
		{
			printf("ERROR; return code from pthread_join() is %d\n",
					return_code);
			exit(-1);
		}
		printf("Main: completed join with thread %ld having a status of %ld\n",
				tid, (long)status);
	}
	
	printf("Main: program completed. Exiting.\n");
	pthread_exit(NULL);
}